unit untFullScreen;

interface

uses
  untMainJonbesh, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, ActnList, jpeg;

type
  TfrmFullScreen = class(TForm)
    Panel1: TPanel;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    lblExit: TLabel;
    imgExit: TImage;
    imgFullScreen: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Panel1DblClick(Sender: TObject);
    procedure actPlayPauseExecute(Sender: TObject);
    procedure actStopExecute(Sender: TObject);
    procedure actRewindExecute(Sender: TObject);
    procedure actForwardExecute(Sender: TObject);
    procedure actPreviousExecute(Sender: TObject);
    procedure actNextExecute(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure actMuteExecute(Sender: TObject);
    procedure imgExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFullScreen: TfrmFullScreen;

implementation

{$R *.dfm}

procedure TfrmFullScreen.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  frmMainJonbesh.MediaPlayer1.Display := frmMainJonbesh.pnlScreen;
end;

procedure TfrmFullScreen.Panel1DblClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmFullScreen.actPlayPauseExecute(Sender: TObject);
begin
  frmMainJonbesh.actPlayPause.OnExecute(Sender);
end;

procedure TfrmFullScreen.actStopExecute(Sender: TObject);
begin
  frmMainJonbesh.actStop.OnExecute(Sender);
end;

procedure TfrmFullScreen.actRewindExecute(Sender: TObject);
begin
  frmMainJonbesh.actRewind.OnExecute(Sender);
end;

procedure TfrmFullScreen.actForwardExecute(Sender: TObject);
begin
  frmMainJonbesh.actForward.OnExecute(Sender);
end;

procedure TfrmFullScreen.actPreviousExecute(Sender: TObject);
begin
  frmMainJonbesh.actPrevious.OnExecute(Sender);
end;

procedure TfrmFullScreen.actNextExecute(Sender: TObject);
begin
  frmMainJonbesh.actNext.OnExecute(Sender);
end;

procedure TfrmFullScreen.actVolHighExecute(Sender: TObject);
begin
  frmMainJonbesh.actVolHigh.OnExecute(Sender);
end;

procedure TfrmFullScreen.actVolLowExecute(Sender: TObject);
begin
  frmMainJonbesh.actVolLow.OnExecute(Sender);
end;

procedure TfrmFullScreen.actMuteExecute(Sender: TObject);
begin
  frmMainJonbesh.actMute.OnExecute(Sender);
end;

procedure TfrmFullScreen.imgExitClick(Sender: TObject);
begin
  Close;
end;

end.
