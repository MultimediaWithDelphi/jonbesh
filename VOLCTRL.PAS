unit VolCtrl;
{ TmVolCtrl: Marco Caselli's Volume Control rel. 1.0
 A component for controlling output volume of a sound card
**********************************************************************
* Feel free to use or give away this software as you see fit.        *
* Please leave the credits in place if you alter the source.         *
*                                                                    *
* This software is delivered to you "as is",                         *
* no guarantees of any kind.                                         *
*                                                                    *
* If you find any bugs, please let me know, I will try to fix them.  *
* If you modify the source code, please send me a copy               *
* Marco Caselli                                                      *
* Web site : http://members.tripod.com/dartclub                      *
* E-mail   : mcaselli@iname.com                                      *
**********************************************************************
*** Sorry for my bad english ...............
 Properties :
      DeviceType : the kind of the device to control, it can be
                   dvCD     : audio Cd-rom
                   dvLineIn : device attached to the linein jack
                             of the sound card
                   dvMidi   : Midi sysntetizer
                   dvWave   : output of the waveform device

      Interval  :  the interval of time (in ms) at which the component check
                   if volume has been chanced from another application;

      Name and Tag: as usual;

      Volume    : the current volume of the associated device.

  Events:

      OnInit    : the event occurs when the conponent associate to a device.
                  here you can for example initiale a visual component, like a
                  trackbar.
      OnChange  : the event occurs at the periodic interval of time specified
                  by the Interval property. here you can include code for
                  updating graphics or other component;

*******************************************************************************}

interface

uses
  Windows,  Messages, SysUtils, Classes,
  Controls, Extctrls, MMSystem;

type
  TDeviceType =(dvCD,dvLineIn,dvMidi,dvWave);

  TVolumeType = record
    case Integer of
    0: (LongVolume: Longint);
    1: (LeftVolume,
        RightVolume : Word);
    end;

  TmVolControl = class(TComponent)
  private
    { Private declarations }
    FDevicetype  : TDeviceType;
    Fdevice      : integer;
    FTimer       : TTimer;
    FonChange    : TNotifyEvent;
    FonInit      : TNotifyEvent;
    FInterval    : word;
    Function  GetVolume: byte ;
    procedure SetVolume(Volume:byte) ;
    procedure SetDeviceType(Value:TDeviceType);
    procedure SetInterval(Value:word);
  protected
    { Protected declarations }
    procedure   VolumeInit;
    procedure   Update(Sender:TObject);
    Procedure   Loaded; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
  published
    { Published declarations }
    property    DeviceType : TDeviceType read FDeviceType write SetDeviceType;
    property    Volume     : byte read GetVolume write SetVolume stored False;
    property    Interval   : word read Finterval write SetInterval default 200;
    property    onChange   : TNotifyEvent read FonChange write FonChange;
    property    onInit     : TNotifyEvent read FonInit write FonInit;

  end;

procedure Register;

implementation

constructor TmVolControl.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
   FTimer          := TTimer.Create(Self);
   FDeviceType     := dvCd;
   FTimer.Interval := 10;
   FTimer.OnTimer  := Update;
   VolumeInit;
   FTimer.Enabled  := True;
end;

destructor  TmVolControl.Destroy;
begin
   FTimer.Free;
   inherited Destroy;
end;

Procedure TmVolControl.Loaded;
begin
  If assigned(FonInit)
     then FonInit(self);
end;

procedure  TmVolControl.SetDeviceType(Value:TDeviceType);
begin
   if FDeviceType <> Value then
      Begin
         FDeviceType := Value;
         VolumeInit;
      end;
end;

procedure  TmVolControl.SetInterval(Value:word);
begin
   if FInterval <> Value then
      Begin
         FInterval := Value;
         FTimer.Interval:=FInterval;
      end;
end;

Procedure TmVolControl.Update(Sender: tobject);
begin
  if assigned(FonChange)
     then FonChange(self);
     Volume:=GetVolume;
end;

procedure TmVolControl.SetVolume(Volume:byte);
 var
   Vol:TVolumeType;
 begin
   Vol.LeftVolume := Volume shl 8;
   Vol.RightVolume := Vol.LeftVolume;

   case FDeviceType of
     dvCD..dvLineIn    : auxSetVolume(FDevice, Vol.LongVolume);
     dvMidi            : midiOutSetVolume(FDevice, Vol.LongVolume);
     dvWave            : waveOutSetVolume(FDevice, Vol.LongVolume);
   end;
 end;

Function TmVolControl.GetVolume: byte ;
var
 Vol:TVolumeType;
begin
  Vol.LongVolume := 0;
  case FDeviceType of
    dvCD..dvLineIn : auxGetVolume(FDevice, @Vol.LongVolume);
    dvMidi         : midiOutGetVolume(FDevice, @Vol.LongVolume);
    dvWave         : waveOutGetVolume(FDevice, @Vol.LongVolume);
  end;
  GetVolume := (Vol.LeftVolume + Vol.RightVolume) shr 9;
end;


procedure TmVolControl.VolumeInit;
var AuxCaps     : TAuxCaps;
    WaveOutCaps : TWaveOutCaps;
    MidiOutCaps : TMidiOutCaps;
    I           : Integer;
 begin
    FDevice:= -1;
    case FDeviceType of
    dvCD: for I := 0 to auxGetNumDevs - 1 do
          begin
             auxGetDevCaps(I, @AuxCaps, SizeOf(AuxCaps));
             if ((AuxCaps.dwSupport and AUXCAPS_VOLUME) <> 0) and
                ((AuxCaps.wTechnology and  AUXCAPS_CDAUDIO <> 0)) then
                begin
                  FTimer.Enabled := True;
                  FDevice := I;
                  Break;
                end;
          end;

    dvLineIn: for I := 0 to auxGetNumDevs - 1 do
          begin
             auxGetDevCaps(I, @AuxCaps, SizeOf(AuxCaps));
             if ((AuxCaps.dwSupport and AUXCAPS_VOLUME) <> 0) and
                ((AuxCaps.wTechnology and  AUXCAPS_AUXIN <> 0 )) then
                begin
                  FTimer.Enabled := True;
                  FDevice := I;
                  Break;
                end;
          end;

    dvWave: for I := 0 to waveOutGetNumDevs - 1 do
            begin
              waveOutGetDevCaps(I, @WaveOutCaps, SizeOf(WaveOutCaps));
              if (WaveOutCaps.dwSupport and WAVECAPS_VOLUME) <> 0 then
              begin
                 FTimer.Enabled := True;
                 FDevice := I;
                 break;
              end;
            end;
    dvMidi:  for I := 0 to midiOutGetNumDevs - 1 do
             begin
                MidiOutGetDevCaps(I, @MidiOutCaps, SizeOf(MidiOutCaps));
                if (MidiOutCaps.dwSupport and MIDICAPS_VOLUME) <> 0 then
                begin
                   FTimer.Enabled := True;
                   FDevice := I;
                   break;
                end;
             end;
    end {case};
   If assigned(FonInit)then FonInit(self);
end;

procedure Register;
begin
  RegisterComponents('MCaselli', [TmVolControl]);
end;
end.
